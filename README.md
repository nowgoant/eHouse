确保有git环境

##1.安装nodejs：

https://nodejs.org/en/  版本v4.4.3 下载安装；

安装是否成功检查：node -v

注：选上添加环境变量

##2.安装 grunt-cli 和 bower

npm install -g bower grunt-cli

或者分别安装

npm install -g bower

npm install -g grunt-cli

安装是否成功检查：

bower -v

grunt -v

##3.安装sass

  http://www.w3cplus.com/sassguide/install.html

##4.安装compass

gem install compass

compass -v

##最后：下载工程基础文件

 git clone https://github.com/nowgoant/eHouse.git MyApp

 cd MyApp

 npm install

##

开发命令：

grunt sync

发布命令：

grunt sync:dist

编译：

grunt build

一般这几个命令就能满足需求了；复杂点就是工程配置（之后更新）。
