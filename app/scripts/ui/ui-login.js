/**
 * Created by nowgoant on 2015/8/9.
 */
var Login = (function () {
  function Login(options) {
    this._tel = '<img src="images/ui-dialog.jpg" class="w100p ui-dialog-img db"/>'
      + '<form action="" class="db w100p ui-dialog-box-form">'
      + '<div class="ui-dialog-box-input auto first">'
      + '<p class="ui-dialog-box-input-l fl h100p">'
      + '<i class="fl icons-h-login-user"></i></p>'
      + '<p class="ui-dialog-box-input-r fl h100p">'
      + '<input type="text" accesskey="u" name="js_user" id="js_user"  class="ui-dialog-form-input fl" autocomplete="off" placeholder="填写用户名"/> '
      + '</p>' + '</div>'
      + '<div class="ui-dialog-box-input auto">'
      + '<p class="ui-dialog-box-input-l fl h100p">'
      + '<i class="fl icons-h-login-ps"></i>'
      + '</p>'
      + '<p class="ui-dialog-box-input-r fl h100p">'
      + '<input type="password" accesskey="u" name="js_ps" id="js_ps"  class="ui-dialog-form-input fl" autocomplete="off" placeholder="填写密码"/>'
      + '</p></div>'
      + '<input type="submit" class="js_login auto  ui-dialog-box-input ui-dialog-box-submit hide" value="立即登录"/>'
      + '<input type="submit" class="js_loginup auto ui-dialog-box-input ui-dialog-box-submit hide" value="立即注册"/> '
      + ' <div class="js_login txt-center ui-dialog-box-sign-link hide">'
      + '<a href="javascript:void(0);">没有账户？立即注册</a></div>'
      + '<div class="js_loginup txt-center ui-dialog-box-sign-link hide">'
      + '<a href="javascript:void(0);">没有账户？立即注册</a></div></form>';

    $.extend(this.options, options);
    this.dialog = new Dialog({
      content: this._tel
    });
  }

  Login.prototype.showLogin = function () {
    this._triggerLogin(true);
    this.dialog.show();
  };
  Login.prototype.showLogup = function () {
    this._triggerLogin(false);
    this.dialog.show();
  };
  Login.prototype.hide = function () {
    this.dialog.hide();
  };
  Login.prototype._triggerLogin = function (isLogin) {
    var $warp = this.dialog.$wrap, $js_loginup = $('.js_loginup', $warp), $js_login = $('.js_login', $warp);
    if (isLogin) {
      $js_login.removeClass('hide');
      $js_loginup.addClass('hide');
    }
    else {
      $js_login.addClass('hide');
      $js_loginup.removeClass('hide');
    }
  };
  return Login;
})();
