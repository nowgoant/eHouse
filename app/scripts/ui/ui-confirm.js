/**
 * Created by nowgoant on 2015/8/9.
 */

var Confirm = (function () {
  function Confirm(options) {
    this.options = {
      dialogClass: '',
      content: '',
      onClickConfirm: null
    };
    this._tel = '<i class="ui-line"></i>' +
      '<p class="ui-title">#{title}</p>' +
      '<p class="ui-desp">#{desp}</p>' +
      '<div class="ui-btn-area">' +
      '<span class="ui-btn-confirm">确定</span><span class="ui-btn-cancel">取消</span>' +
      '</div>';

    $.extend(this.options, options);

    this.options.dialogClass += ' ui-confirm';
    this.options.content = $.template.bindData(this._tel, this.options);

    this.dialog = new Dialog(this.options);
  }

  Confirm.prototype = {
    _bindEvent: function () {
      var _this = this;
      var opts = this.options;
      var $wrap = _this.dialog.$wrap;

      $('.ui-btn-confirm', $wrap).click(function () {
        if ($.isFunction(opts.onClickConfirm)) {
          opts.onClickConfirm();
        }
        _this.hide();
      });

      $('.ui-btn-cancel', $wrap).click(function () {
        _this.hide();
      });
    },
    show: function () {
      this._bindEvent();

      this.dialog.show();
    },
    hide: function () {
      this.dialog.hide();
    }
  };

  return Confirm;
}());
