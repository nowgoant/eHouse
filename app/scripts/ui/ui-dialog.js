/**
 * Created by nowgoant on 2015/8/9.
 */
var Dialog = (function () {
    function Dialog(options) {
        this._tel = '<div class="ui-dialog"><div class="ui-dialog-box #{dialogClass}">#{content}</div></div>';
        this.options = {};
        $.extend(this.options, options);
        var html = $.template.bindData(this._tel, this.options);
        console.log(html);
        this.$wrap = $(html);
    }
    Dialog.prototype.show = function () {
        this._bindEvent();
        $('body').append(this.$wrap);
        this.$wrap.removeClass('hide');
        this.$wrap.fadeIn();
    };
    Dialog.prototype.hide = function () {
        var dialog = this.$wrap;
        dialog.fadeOut();
        $.delay(function () {
            dialog.remove();
        }, 200);
    };
    Dialog.prototype._bindEvent = function () {
        var _this = this;
        var $dialogContent=$('.ui-dialog-box',_this.$wrap);

        this.$wrap.on('click', function () {
            _this.hide();
        });

        $dialogContent.click(function(evt){
            $.stopEvt(evt);
        });
    };
    return Dialog;
})();
