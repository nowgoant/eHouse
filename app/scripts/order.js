/**
 * Created by nowgoant on 2015/12/12.
 */

(function ($) {
  function bindEvent() {
    $('.js_order_delte').click(function () {
      var confirm = new Confirm({
        title: '删除商品',
        desp: '确认要删除该商品吗？',
        onClickConfirm: function () {
          alert('confirm');
        }
      });

      confirm.show();
    });
  }

  $(document).ready(function () {
    bindEvent();
  });
}(jQuery));
