
(function(win,$){
  'use strict';

function init(){
   new Swiper('.swiper-container', {
          loop : true,
          pagination: '.swiper-pagination',
          paginationClickable: true,
          nextButton: '.swiper-button-next',
          prevButton: '.swiper-button-prev'
      });
}

  $(function(){
   init();
  });
}(window,jQuery));
