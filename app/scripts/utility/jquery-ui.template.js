/**
 * Created by majun1 on 2015/3/9.
 */
(function ($) {
    'use strict';
    var _temp = {
        bindData: function (template, data) {
            if (data && $.isPlainObject(data) && $.isString(template)) {
                template = template.replace(/#\{(.+?)\}/g, function (n, t) {
                    var keys;
                    keys = t.split('|');
                    //console.log(data[keys[0]]);
                    return $.toSafeString(data[keys[0]]);
                });
            }
            return template;
        }
    };
    $.template = _temp;
}(jQuery));
