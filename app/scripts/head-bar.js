/// <reference path="ui/ui-login.ts" />
(function ($) {
    var login = null;
    function createLogin() {
        if (!login) {
            login = new Login();
        }
    }
    $('#js_login').click(function () {
        createLogin();
        if (login) {
            login.showLogin();
        }
    });
    $('#js_login_up').click(function () {
        createLogin();
        if (login) {
            login.showLogup();
        }
    });
    console.log('hello');
}(jQuery));
